import numpy as np
import math
a=np.array([(8,10),(9,7),(1,1),(4,1),(1.5,1.5),(1,2),(2,4),(5,2),(3,1),(4,1),(5,1),(4,2),(3,8),(4,6),(5,5),(7,2),(6,9),(8,2),(4,3)])
eps=4
minpts=5
label=[0]*len(a)
n=[]
def regionquery(i,eps):
    distance = []
    for j in range(0,len(a)):
        dis = math.sqrt(sum([(a - b) ** 2 for a, b in zip(a[j], a[i])]))
        if dis < eps:
            distance.append(a[j])
    return distance
def expandcluster(i,n,cluster,eps,minpts):
    label[i]=cluster
    for k in range(0,len(n)):
        if label[k]==0:
            neighborpts=regionquery(i,eps)
            if(len(neighborpts))>=minpts :
                n = n + neighborpts
        if label[k]==0:
            label[k]=cluster
def dbscan(a,eps,minpts):
    cluster=0
    for i in range(0,len(a)):
        if label[i]==0:
            n = regionquery(i,eps)
            if len(n)<minpts:
                label[i]=-1
            else:
                cluster=cluster+1
                expandcluster(i,n,cluster,eps,minpts)
    print (label)
dbscan(a,eps,minpts)


